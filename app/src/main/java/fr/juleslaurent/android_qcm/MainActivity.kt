package fr.juleslaurent.android_qcm

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.findNavController
import fr.juleslaurent.android_qcm.model.Question
import fr.juleslaurent.android_qcm.service.ApiInterface
import fr.juleslaurent.android_qcm.ui.question.QuestionFragment
import fr.juleslaurent.android_qcm.ui.question.QuestionFragmentDirections
import kotlinx.android.synthetic.main.fragment_question.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), QuestionFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.getData();

    }

    private var questions : MutableList<Question> = arrayListOf<Question>();


    private fun getData() {
        ApiInterface.instance.getQuestions().enqueue(this.onQuestionsReceived);
    }

    private val onQuestionsReceived = object : Callback<List<Question>> {
        override fun onResponse(call: Call<List<Question>>, response: Response<List<Question>>) {
            if (response.body() != null && response.code() == 200) {
                questions = response.body() as MutableList<Question>;

                val action = QuestionFragmentDirections.navigateToQuestion(questions.get(0));
                 this@MainActivity.findNavController(R.id.nav_host_fragment).navigate(action);
            }
        }

        override fun onFailure(call: Call<List<Question>>, t: Throwable) {
            Log.e("HTTP FAILURE", t.message!!)
        }
    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
