package fr.juleslaurent.android_qcm.model

import java.io.Serializable

class Question : Serializable {
    var question: String? = null
    var choices: ArrayList<QuestionChoice> = arrayListOf<QuestionChoice>();
    var validAnswers: ArrayList<Int> = arrayListOf<Int>()
    var multipleChoices: Boolean = true;
}