package fr.juleslaurent.android_qcm.service

import com.google.gson.GsonBuilder
import fr.juleslaurent.android_qcm.model.Question
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiInterface {
    @GET("api_call.php")
    fun getQuestions() : Call<List<Question>>


    companion object {
        val instance: ApiInterface by lazy {
            val gsonBuilder = GsonBuilder();
            val gson = gsonBuilder.create();
            val retrofit = Retrofit.Builder()
                .baseUrl("http://student.ddadev.fr/dev_mobile/EVALUATION_J5/jules_laurent/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
            retrofit.create(ApiInterface::class.java)
        }
    }
}