package fr.juleslaurent.android_qcm.ui.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.juleslaurent.android_qcm.R

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
}
