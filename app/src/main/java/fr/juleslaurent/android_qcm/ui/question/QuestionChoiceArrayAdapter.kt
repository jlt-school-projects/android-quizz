package fr.juleslaurent.android_qcm.ui.question

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.view.LayoutInflater
import android.widget.TextView
import fr.juleslaurent.android_qcm.R
import fr.juleslaurent.android_qcm.model.QuestionChoice


class QuestionChoiceArrayAdapter: ArrayAdapter<QuestionChoice> {
    constructor(context: Context, resource: Int, objects: MutableList<QuestionChoice>) : super(
        context,
        resource,
        objects
    )

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val choice : QuestionChoice? = getItem(position)
        var view = convertView;

        // Check if an existing view is being reused, otherwise inflate the view

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.question_choices_item, parent, false)
        } else {
            view = convertView;
        }

        view?.findViewById<TextView>(R.id.question_choice_text)?.text = choice?.label;

        // Return the completed view to render on screen

        return view!!
    }
}