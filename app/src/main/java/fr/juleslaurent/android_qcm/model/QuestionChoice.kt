package fr.juleslaurent.android_qcm.model

import java.io.Serializable

class QuestionChoice : Serializable {
    var id: Int? = null
    var label: String? = null
}